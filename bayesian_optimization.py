import warnings, numpy as np, pandas as pd, matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from bayes_opt import BayesianOptimization
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier

x_train = None
y_train = None
x_dev = None
y_dev = None

def binarize_continious_data(data):
    #This fuction I create multiple ranges for all continious variables and each age to that group. 
    #After doing this I drop the existing column
    age_group = []
    for age in data["age"]:
        if age < 25:
            age_group.append("<25")
        elif 25 <= age <= 34:
            age_group.append("25-34")
        elif 34 < age <= 44:
            age_group.append("35-44")
        elif 44 < age <= 54:
            age_group.append("45-54")
        elif 54 < age <= 65:
            age_group.append("55-64")
        else:
            age_group.append("65 and over")

    income_df = data.copy()
    income_df["age_group"] = age_group
    del income_df["age"]
    
    workhours_group = []
    for workhours in income_df["workhours"]:
        if workhours < 25:
            workhours_group.append("<25")
        elif 26 <= workhours <= 50:
            workhours_group.append("26-50")
        elif 50 < workhours <= 75:
            workhours_group.append("51-75")
        else:
            workhours_group.append("76 and over")

    new_income_df = income_df.copy()
    new_income_df["workhours_group"] = workhours_group
    del new_income_df["workhours"]
    
    return new_income_df

def data_preprocessing(data):
    #Here I binarize all the continious and catagorical data. As I am binarizing I don't need a scalar fit.
    #I also return the values of x_train and y_train.
    #Also converting the 0 or 1 class in y_train to -1 or 1.
    data = binarize_continious_data(data)
    data = pd.get_dummies(data, drop_first=True)
    return data.loc[:, data.columns != 'income_ >50K'], data['income_ >50K'].map({0: -1, 1: 1})

def bag_objective(max_depth, n_estimators):
    global x_train, x_dev, y_train, y_dev
    cls = BaggingClassifier(DecisionTreeClassifier(max_depth=int(max_depth)), n_estimators=int(n_estimators))
    cls.fit(x_train, y_train)
    return cls.score(x_dev, y_dev)

def boost_objective(max_depth, n_estimators):
    global x_train, x_dev, y_train, y_dev
    cls = AdaBoostClassifier(DecisionTreeClassifier(max_depth=int(max_depth)), algorithm="SAMME", n_estimators=int(n_estimators))
    cls.fit(x_train, y_train)
    return cls.score(x_dev, y_dev)

def main():
    global x_train, y_train, x_dev, y_dev, x_test, y_test
    print('\nBayesian Optimization output: ')
    cols = ["age", "workclass", "education", "relationship", "profession", "race", "gender", "workhours", "nationality", "income"]
    dev_data = pd.read_csv('income.dev.txt', header = None, names = cols)
    train_data = pd.read_csv('income.train.txt', header = None, names = cols)

    #Doing the data preprocessing over the given data
    data = pd.concat([train_data, dev_data])
    X, Y = data_preprocessing(data)

    x_train, y_train = X[:len(train_data)], Y[:len(train_data)]
    x_dev, y_dev = X[len(train_data):len(dev_data)+len(train_data)], Y[len(train_data):len(dev_data)+len(train_data)]

    plt.style.use('ggplot')
    fig, axs = plt.subplots(1, 2, figsize=(10, 5), dpi=300, facecolor='w', edgecolor='k', sharex=True)

    count = 0
    for objective in [bag_objective, boost_objective]:
        bo = BayesianOptimization(objective, {'max_depth': (1, 10), 'n_estimators': (10, 100)})
        bo.maximize(init_points=1, n_iter=49, acq='ei')
        axs[count].plot(range(50), bo.Y)
        if not count:
            axs[count].set_title('Bagging')
        else:
            axs[count].set_title('Boosting')
        axs[count].set_ylabel('Accuracy')
        axs[count].set_xlabel('Iterations')
        count += 1

    fig.suptitle('Accuracy after Bayseian Optimization')
    plt.savefig('BayesianOptimization.png')

if __name__ == "__main__": 
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        main()